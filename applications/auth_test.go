package applications

import (
	"crypto/rand"
	"cwtch.im/tapir"
	"cwtch.im/tapir/primitives"
	"encoding/json"
	"golang.org/x/crypto/ed25519"
	"testing"
)

type MockConnection struct {
	id       primitives.Identity
	outbound bool
}

func (mc *MockConnection) Init(outbound bool) {
	mc.id, _ = primitives.InitializeEphemeralIdentity()
	mc.outbound = outbound
	return
}

func (mc MockConnection) Hostname() string {
	return mc.id.Hostname()
}

func (mc MockConnection) IsOutbound() bool {
	return mc.outbound
}

func (mc MockConnection) ID() *primitives.Identity {
	return &mc.id
}

func (mc MockConnection) Expect() []byte {
	longTermPubKey := ed25519.PublicKey(mc.id.PublicKeyBytes())
	epk, _, _ := ed25519.GenerateKey(rand.Reader)
	ephemeralPublicKey := ed25519.PublicKey(epk)
	//ephemeralPrivateKey := ed25519.PrivateKey(esk)
	//ephemeralIdentity := identity.InitializeV3("", &ephemeralPrivateKey, &ephemeralPublicKey)
	authMessage := AuthMessage{LongTermPublicKey: longTermPubKey, EphemeralPublicKey: ephemeralPublicKey}
	serialized, _ := json.Marshal(authMessage)
	return serialized
}

func (MockConnection) SetHostname(hostname string) {
	panic("implement me")
}

func (MockConnection) HasCapability(name string) bool {
	panic("implement me")
}

func (MockConnection) SetCapability(name string) {
	panic("implement me")
}

func (MockConnection) SetEncryptionKey(key [32]byte) {
	// no op
}

func (MockConnection) Send(message []byte) {
	// no op
}

func (MockConnection) Close() {
	// no op
}

func (MockConnection) App() tapir.Application {
	// no op
	return nil
}

func (MockConnection) IsClosed() bool {
	panic("implement me")
}

func TestAuthApp_Failed(t *testing.T) {
	var authApp AuthApp
	ai := authApp.NewInstance()

	mc := new(MockConnection)
	mc.Init(true)
	ai.Init(mc)
}
