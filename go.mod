module cwtch.im/tapir

require (
	git.openprivacy.ca/openprivacy/libricochet-go v1.0.4
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f
)
