package primitives

import (
	"time"
)

// TimeProvider is an interface used by services to timestamp events. Why not just have them use time.Now()? We want
// to be able to write tests that simulate behavior over several hours, and thus having an interface to abstract away
// time details for the services is very useful.
type TimeProvider interface {
	GetCurrentTime() time.Time
}

// OSTimeProvider provides a wrapper around time provider which simply provides the time as given by the operating system.
type OSTimeProvider struct {
}

// GetCurrentTime returns the time provided by the OS
func (ostp OSTimeProvider) GetCurrentTime() time.Time {
	return time.Now()
}
