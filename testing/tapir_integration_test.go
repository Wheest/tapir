package testing

import (
	"cwtch.im/tapir"
	"cwtch.im/tapir/applications"
	"cwtch.im/tapir/networks/tor"
	"cwtch.im/tapir/primitives"
	"git.openprivacy.ca/openprivacy/libricochet-go/connectivity"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"golang.org/x/crypto/ed25519"
	"runtime"
	"sync"
	"testing"
	"time"
)

// SimpleApp is a trivial implementation of a basic p2p application
type SimpleApp struct {
	applications.AuthApp
}

// NewInstance should always return a new instantiation of the application.
func (ea SimpleApp) NewInstance() tapir.Application {
	return new(SimpleApp)
}

// Init is run when the connection is first started.
func (ea SimpleApp) Init(connection tapir.Connection) {
	// First run the Authentication App
	ea.AuthApp.Init(connection)

	if connection.HasCapability(applications.AuthCapability) {
		// The code for out simple application (We just send and receive "Hello"
		connection.Send([]byte("Hello"))
		message := connection.Expect()
		log.Infof("Received: %q", message)
	}
}

var AuthSuccess = false

// CheckConnection is a simple test that GetConnection is working.
func CheckConnection(service tapir.Service, hostname string, group *sync.WaitGroup) {
	for {
		_, err := service.GetConnection(hostname)
		if err == nil {
			log.Infof("Authed!")
			group.Done()
			return
		}
		log.Infof("Waiting for Authentication...%v", err)
		time.Sleep(time.Second * 5)
	}
}

func TestTapir(t *testing.T) {

	numRoutinesStart := runtime.NumGoroutine()
	log.SetLevel(log.LevelDebug)
	log.Infof("Number of goroutines open at start: %d", runtime.NumGoroutine())
	// Connect to Tor
	var acn connectivity.ACN
	acn, _ = connectivity.StartTor("./", "")
	acn.WaitTillBootstrapped()

	// Generate Server Keys
	id, sk := primitives.InitializeEphemeralIdentity()

	// Init the Server running the Simple App.
	var service tapir.Service
	service = new(tor.BaseOnionService)
	service.Init(acn, sk, &id)

	// Goroutine Management
	sg := new(sync.WaitGroup)
	sg.Add(1)
	go func() {
		service.Listen(SimpleApp{})
		sg.Done()
	}()

	// Wait for server to come online
	time.Sleep(time.Second * 30)
	wg := new(sync.WaitGroup)
	wg.Add(2)
	// Init a Client to Connect to the Server
	client, clienthostname := genclient(acn)
	go connectclient(client, id.PublicKey(), wg)
	CheckConnection(service, clienthostname, wg)
	wg.Wait()
	// Wait for Server to Sync
	time.Sleep(time.Second * 2)
	log.Infof("Closing ACN...")
	acn.Close()
	sg.Wait()
	time.Sleep(time.Second * 2)
	log.Infof("Number of goroutines open at close: %d", runtime.NumGoroutine())
	if numRoutinesStart != runtime.NumGoroutine() {
		t.Errorf("Potential goroutine leak: Num Start:%v NumEnd: %v", numRoutinesStart, runtime.NumGoroutine())
	}
	if !AuthSuccess {
		t.Fatalf("Integration Test FAILED, client did not auth with server")
	}
}

func genclient(acn connectivity.ACN) (tapir.Service, string) {
	id, sk := primitives.InitializeEphemeralIdentity()
	var client tapir.Service
	client = new(tor.BaseOnionService)
	client.Init(acn, sk, &id)
	return client, id.Hostname()
}

// Client will Connect and launch it's own Echo App goroutine.
func connectclient(client tapir.Service, key ed25519.PublicKey, group *sync.WaitGroup) {
	client.Connect(utils.GetTorV3Hostname(key), SimpleApp{})

	// Once connected, it shouldn't take long to authenticate and run the application. So for the purposes of this demo
	// we will wait a little while then exit.
	time.Sleep(time.Second * 5)

	conn, _ := client.GetConnection(utils.GetTorV3Hostname(key))
	log.Debugf("Client has Auth: %v", conn.HasCapability(applications.AuthCapability))
	AuthSuccess = true
	group.Done()
}
