package testing

import (
	"cwtch.im/tapir"
	"cwtch.im/tapir/applications"
	"cwtch.im/tapir/networks/tor"
	"cwtch.im/tapir/primitives"
	"git.openprivacy.ca/openprivacy/libricochet-go/connectivity"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"golang.org/x/crypto/ed25519"
	"runtime"
	"sync"
	"testing"
	"time"
)

func TestTapirMaliciousRemote(t *testing.T) {

	numRoutinesStart := runtime.NumGoroutine()
	log.SetLevel(log.LevelDebug)
	log.Infof("Number of goroutines open at start: %d", runtime.NumGoroutine())
	// Connect to Tor
	var acn connectivity.ACN
	acn, _ = connectivity.StartTor("./", "")
	acn.WaitTillBootstrapped()

	// Generate Server Keys, not we generate two sets
	id, _ := primitives.InitializeEphemeralIdentity()
	id2, sk2 := primitives.InitializeEphemeralIdentity()

	// Init the Server running the Simple App.
	var service tapir.Service
	service = new(tor.BaseOnionService)
	// Initialize an onion service with one identity, but the auth app with another, this should
	// trigger a failure in authentication protocol
	service.Init(acn, sk2, &id)

	// Goroutine Management
	sg := new(sync.WaitGroup)
	sg.Add(1)
	go func() {
		service.Listen(applications.AuthApp{})
		sg.Done()
	}()

	// Wait for server to come online
	time.Sleep(time.Second * 30)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	// Init a Client to Connect to the Server
	log.Infof("initializing the client....")
	client, _ := genclient(acn)
	go connectclientandfail(client, id2.PublicKey(), wg, t)
	wg.Wait()
	// Wait for Server to Sync
	time.Sleep(time.Second * 2)
	log.Infof("closing ACN...")
	acn.Close()
	sg.Wait()
	time.Sleep(time.Second * 2)
	log.Infof("Number of goroutines open at close: %d", runtime.NumGoroutine())
	if numRoutinesStart != runtime.NumGoroutine() {
		t.Errorf("Potential goroutine leak: Num Start:%v NumEnd: %v", numRoutinesStart, runtime.NumGoroutine())
	}
}

// Client will Connect and launch it's own Echo App goroutine.
func connectclientandfail(client tapir.Service, key ed25519.PublicKey, group *sync.WaitGroup, t *testing.T) {
	client.Connect(utils.GetTorV3Hostname(key), applications.AuthApp{})

	// Once connected, it shouldn't take long to authenticate and run the application. So for the purposes of this demo
	// we will wait a little while then exit.
	time.Sleep(time.Second * 5)

	log.Infof("Checking connection status...")
	conn, err := client.GetConnection(utils.GetTorV3Hostname(key))
	if err == nil {
		group.Done()
		t.Fatalf("Connection should have failed! %v %v", conn, err)
	}
	log.Infof("Successfully failed to authenticate...")
	group.Done()
}
