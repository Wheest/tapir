#!/bin/bash

set -e
pwd
go test ${1} -coverprofile=applications.cover.out -v ./applications
echo "mode: set" > coverage.out && cat *.cover.out | grep -v mode: | sort -r | \
awk '{if($1 != last) {print $0;last=$1}}' >> coverage.out
rm -rf *.cover.out
