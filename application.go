package tapir

// Application defines the interface for all Tapir Applications
type Application interface {
	NewInstance() Application
	Init(connection Connection)
}
